<?php

Route::group(['name' => 'feedback', 'groupName' => 'feedback'], function () {
    Route::get('/feedback', 'APIControllers\PluginsControllers\FeedbackAPIController@getAllFeedback')->name('getAllFeedback');
    Route::get('/feedback/{id}', 'APIControllers\PluginsControllers\FeedbackAPIController@getFeedback')->name('getFeedback');
});
