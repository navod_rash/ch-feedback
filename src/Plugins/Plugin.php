<?php

namespace Creativehandles\ChFeedback\Plugins;
use App\Interfaces\PluginInterface;

class Plugin implements PluginInterface {

    public $config;

    public function __construct()
    {
        $this->config = config('plugins');
    }

    /*
     * Get name of called class without namespaces
     */
    public function GetCalledClass()
    {
        $c = str_replace(__NAMESPACE__ . '\\', '', get_called_class());
        $c = explode('\\', $c);

        return end($c);
    }

    public function GetPluginTitle()
    {
        return $this->config[$this->GetCalledClass()]['Title'];
    }

    public function GetPluginVersion()
    {
        return $this->config[$this->GetCalledClass()]['Version'];
    }

}