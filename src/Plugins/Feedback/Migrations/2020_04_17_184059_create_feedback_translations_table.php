<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // create the translations table
        if (! Schema::hasTable('feedback_translations')) {
            Schema::create('feedback_translations', function (Blueprint $table) {
                $table->increments('id');
                $table->unsignedInteger('feedback_id');
                $table->string('locale', 10)->index();
                $table->longText('text');
                $table->timestamps();

                $table->unique(['feedback_id', 'locale']);
                $table->foreign('feedback_id')->references('id')->on('feedbacks')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('feedback_translations');
    }
}
