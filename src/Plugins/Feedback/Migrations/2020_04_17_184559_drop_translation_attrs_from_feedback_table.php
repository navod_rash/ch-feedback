<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTranslationAttrsFromFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // we drop the translation attributes in our main table:
        Schema::table('feedbacks', function ($table) {
            $table->dropColumn('text');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // add the translatable attributes back in to main table
        Schema::table('feedbacks', function ($table) {
            $table->longText('text')->after('avatar');
        });
    }
}
